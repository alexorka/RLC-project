﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LRC_NET_Framework;
using LRC_NET_Framework.Models;

namespace LRC_NET_Framework.Controllers
{
    [Route("Admin/EditSemesters/{action}")]
    public class AdminEditSemestersController : Controller
    {
        private LRCEntities db = new LRCEntities();

        // GET: Semesters
        public ActionResult Index()
        {
            return View(db.tb_Semesters.ToList());
        }

        // GET: Semesters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_Semesters tb_Semesters = db.tb_Semesters.Find(id);
            if (tb_Semesters == null)
            {
                return HttpNotFound();
            }
            return View(tb_Semesters);
        }

        // GET: Semesters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Semesters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SemesterModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.SemesterStartDate > model.SemesterEndDate)
                {
                    ModelState.AddModelError("", "End Date cannot be less than Start Date.");
                    return View(model);
                }
                tb_Semesters semester = new tb_Semesters();
                semester.SemesterYear = model.SemesterStartDate.Year.ToString();
                string season = "Spring";
                if (model.SemesterStartDate.Month >= 6 && model.SemesterStartDate.Month <= 8) //Summer
                    season = "Summer";
                else if (model.SemesterStartDate.Month >= 8 && model.SemesterStartDate.Month <= 12) //Fall
                    season = "Fall";
                semester.SemesterName = season;
                semester.SemesterStartDate = model.SemesterStartDate;
                semester.SemesterEndDate = model.SemesterEndDate;

                db.tb_Semesters.Add(semester);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Semesters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_Semesters tb_Semesters = db.tb_Semesters.Find(id);
            if (tb_Semesters == null)
            {
                return HttpNotFound();
            }
            return View(tb_Semesters);
        }

        // POST: Semesters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SemesterID,SemesterName,SemesterYear,SemesterStartDate,SemesterEndDate")] tb_Semesters tb_Semesters)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tb_Semesters).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tb_Semesters);
        }

        // GET: Semesters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_Semesters tb_Semesters = db.tb_Semesters.Find(id);
            if (tb_Semesters == null)
            {
                return HttpNotFound();
            }
            return View(tb_Semesters);
        }

        // POST: Semesters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tb_Semesters tb_Semesters = db.tb_Semesters.Find(id);
            db.tb_Semesters.Remove(tb_Semesters);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
