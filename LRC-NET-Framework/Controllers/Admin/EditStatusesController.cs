﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LRC_NET_Framework;

namespace LRC_NET_Framework.Controllers
{
    [Route("Admin/EditStatuses/{action}")]
    public class AdminEditStatusesController : Controller
    {
        private LRCEntities db = new LRCEntities();

        // GET: AdminEditStatuses
        public ActionResult Index()
        {
            return View(db.tb_Categories.ToList());
        }

        // GET: AdminEditStatuses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_Categories tb_Categories = db.tb_Categories.Find(id);
            if (tb_Categories == null)
            {
                return HttpNotFound();
            }
            return View(tb_Categories);
        }

        // GET: AdminEditStatuses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminEditStatuses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryName")] tb_Categories status)
        {
            if (ModelState.IsValid)
            {
                if (db.tb_Categories.Where(s => s.CategoryName.ToUpper() == status.CategoryName.ToUpper()).Count() > 0)
                {
                    ModelState.AddModelError("", "This status is already in the table.");
                    return View(status);
                }
                else
                {
                    db.tb_Categories.Add(status);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(status);
        }

        // GET: AdminEditStatuses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            tb_Categories tb_Categories = db.tb_Categories.Find(id);

            if (tb_Categories == null)
            {
                return HttpNotFound();
            }
            return View(tb_Categories);
        }

        // POST: AdminEditStatuses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryID,CategoryName")] tb_Categories status)
        {
            if (ModelState.IsValid)
            {
                if (status.CategoryID == 4)
                {
                    ModelState.AddModelError("", "Default value cannot be edited");
                    return View(status);
                }
                db.Entry(status).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(status);
        }

        // GET: AdminEditStatuses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_Categories tb_Categories = db.tb_Categories.Find(id);
            if (tb_Categories == null)
            {
                return HttpNotFound();
            }
            return View(tb_Categories);
        }

        // POST: AdminEditStatuses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tb_Categories tb_Categories = db.tb_Categories.Find(id);
            if (id == 4)
            {
                ModelState.AddModelError("", "Default value cannot be deleted");
                return View(tb_Categories);
            }
            db.tb_Categories.Remove(tb_Categories);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
